package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class GeographicCoordinates{

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("longitude")
	private String longitude;

	public String getLatitude(){
		return latitude;
	}

	public String getLongitude(){
		return longitude;
	}
}