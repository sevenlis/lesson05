
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

public class Day {

    @SerializedName("dayCode")
    public String dayCode;
    @SerializedName("openingTime")
    public String openingTime;
    @SerializedName("closingTime")
    public String closingTime;
    @SerializedName("Break")
    public Break _break;

    public String getDayCode() {
        return dayCode;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public String getClosingTime() {
        return closingTime;
    }

    public Break getBreak() {
        return _break;
    }
}
