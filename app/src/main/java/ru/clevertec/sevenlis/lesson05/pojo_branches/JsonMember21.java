package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class JsonMember21{

	@SerializedName("dateTime")
	private String dateTime;

	@SerializedName("currentStatus")
	private String currentStatus;

	@SerializedName("segment")
	private String segment;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("serviceId")
	private String serviceId;

	@SerializedName("type")
	private String type;

	@SerializedName("url")
	private String url;

	public String getDateTime(){
		return dateTime;
	}

	public String getCurrentStatus(){
		return currentStatus;
	}

	public String getSegment(){
		return segment;
	}

	public String getName(){
		return name;
	}

	public String getDescription(){
		return description;
	}

	public String getServiceId(){
		return serviceId;
	}

	public String getType(){
		return type;
	}

	public String getUrl(){
		return url;
	}
}