
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.clevertec.sevenlis.lesson05.RootItem;
import ru.clevertec.sevenlis.lesson05.MarkerItem;

public class AtmsRoot implements RootItem {

    @SerializedName("Data")
    public Data data;

    public Data getData() {
        return data;
    }

    @Override
    public List<MarkerItem> getMarkerItems() {
        List<MarkerItem> markerItems = new ArrayList<>();
        for (Atm atm : getData().getAtms()) {
            markerItems.add(new MarkerItem(atm));
        }
        return markerItems;
    }
}
