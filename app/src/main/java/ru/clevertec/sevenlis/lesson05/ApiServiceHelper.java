package ru.clevertec.sevenlis.lesson05;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceHelper {
    private static ApiServiceHelper mInstance;
    private static final String BASE_URL = "https://belarusbank.by/open-banking/v1.0/";
    private final Retrofit mRetrofit;

    public ApiServiceHelper() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
    }

    public ApiService getApiService() {
        return mRetrofit.create(ApiService.class);
    }

    public static ApiServiceHelper getInstance() {
        if (mInstance == null)
            mInstance = new ApiServiceHelper();
        return mInstance;
    }
}
