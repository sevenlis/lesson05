package ru.clevertec.sevenlis.lesson05.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Service{

	@SerializedName("88")
	private JsonMember88 jsonMember88;

	@SerializedName("89")
	private JsonMember89 jsonMember89;

	@SerializedName("90")
	private JsonMember90 jsonMember90;

	@SerializedName("91")
	private JsonMember91 jsonMember91;

	@SerializedName("92")
	private JsonMember92 jsonMember92;

	@SerializedName("93")
	private JsonMember93 jsonMember93;

	@SerializedName("94")
	private JsonMember94 jsonMember94;

	@SerializedName("95")
	private JsonMember95 jsonMember95;

	@SerializedName("96")
	private JsonMember96 jsonMember96;

	@SerializedName("97")
	private JsonMember97 jsonMember97;

	@SerializedName("10")
	private JsonMember10 jsonMember10;

	@SerializedName("98")
	private JsonMember98 jsonMember98;

	@SerializedName("11")
	private JsonMember11 jsonMember11;

	@SerializedName("99")
	private JsonMember99 jsonMember99;

	@SerializedName("12")
	private JsonMember12 jsonMember12;

	@SerializedName("13")
	private JsonMember13 jsonMember13;

	@SerializedName("14")
	private JsonMember14 jsonMember14;

	@SerializedName("15")
	private JsonMember15 jsonMember15;

	@SerializedName("16")
	private JsonMember16 jsonMember16;

	@SerializedName("17")
	private JsonMember17 jsonMember17;

	@SerializedName("18")
	private JsonMember18 jsonMember18;

	@SerializedName("19")
	private JsonMember19 jsonMember19;

	@SerializedName("0")
	private JsonMember0 jsonMember0;

	@SerializedName("1")
	private JsonMember1 jsonMember1;

	@SerializedName("2")
	private JsonMember2 jsonMember2;

	@SerializedName("3")
	private JsonMember3 jsonMember3;

	@SerializedName("4")
	private JsonMember4 jsonMember4;

	@SerializedName("5")
	private JsonMember5 jsonMember5;

	@SerializedName("6")
	private JsonMember6 jsonMember6;

	@SerializedName("7")
	private JsonMember7 jsonMember7;

	@SerializedName("8")
	private JsonMember8 jsonMember8;

	@SerializedName("9")
	private JsonMember9 jsonMember9;

	@SerializedName("20")
	private JsonMember20 jsonMember20;

	@SerializedName("21")
	private JsonMember21 jsonMember21;

	@SerializedName("22")
	private JsonMember22 jsonMember22;

	@SerializedName("23")
	private JsonMember23 jsonMember23;

	@SerializedName("24")
	private JsonMember24 jsonMember24;

	@SerializedName("25")
	private JsonMember25 jsonMember25;

	@SerializedName("26")
	private JsonMember26 jsonMember26;

	@SerializedName("27")
	private JsonMember27 jsonMember27;

	@SerializedName("28")
	private JsonMember28 jsonMember28;

	@SerializedName("29")
	private JsonMember29 jsonMember29;

	@SerializedName("30")
	private JsonMember30 jsonMember30;

	@SerializedName("31")
	private JsonMember31 jsonMember31;

	@SerializedName("32")
	private JsonMember32 jsonMember32;

	@SerializedName("33")
	private JsonMember33 jsonMember33;

	@SerializedName("34")
	private JsonMember34 jsonMember34;

	@SerializedName("35")
	private JsonMember35 jsonMember35;

	@SerializedName("36")
	private JsonMember36 jsonMember36;

	@SerializedName("37")
	private JsonMember37 jsonMember37;

	@SerializedName("38")
	private JsonMember38 jsonMember38;

	@SerializedName("39")
	private JsonMember39 jsonMember39;

	@SerializedName("CurrencyExchange")
	private List<CurrencyExchangeItem> currencyExchange;

	@SerializedName("40")
	private JsonMember40 jsonMember40;

	@SerializedName("41")
	private JsonMember41 jsonMember41;

	@SerializedName("42")
	private JsonMember42 jsonMember42;

	@SerializedName("43")
	private JsonMember43 jsonMember43;

	@SerializedName("44")
	private JsonMember44 jsonMember44;

	@SerializedName("45")
	private JsonMember45 jsonMember45;

	@SerializedName("46")
	private JsonMember46 jsonMember46;

	@SerializedName("47")
	private JsonMember47 jsonMember47;

	@SerializedName("48")
	private JsonMember48 jsonMember48;

	@SerializedName("49")
	private JsonMember49 jsonMember49;

	@SerializedName("50")
	private JsonMember50 jsonMember50;

	@SerializedName("51")
	private JsonMember51 jsonMember51;

	@SerializedName("52")
	private JsonMember52 jsonMember52;

	@SerializedName("53")
	private JsonMember53 jsonMember53;

	@SerializedName("54")
	private JsonMember54 jsonMember54;

	@SerializedName("55")
	private JsonMember55 jsonMember55;

	@SerializedName("56")
	private JsonMember56 jsonMember56;

	@SerializedName("57")
	private JsonMember57 jsonMember57;

	@SerializedName("58")
	private JsonMember58 jsonMember58;

	@SerializedName("59")
	private JsonMember59 jsonMember59;

	@SerializedName("60")
	private JsonMember60 jsonMember60;

	@SerializedName("61")
	private JsonMember61 jsonMember61;

	@SerializedName("62")
	private JsonMember62 jsonMember62;

	@SerializedName("63")
	private JsonMember63 jsonMember63;

	@SerializedName("64")
	private JsonMember64 jsonMember64;

	@SerializedName("65")
	private JsonMember65 jsonMember65;

	@SerializedName("66")
	private JsonMember66 jsonMember66;

	@SerializedName("67")
	private JsonMember67 jsonMember67;

	@SerializedName("68")
	private JsonMember68 jsonMember68;

	@SerializedName("69")
	private JsonMember69 jsonMember69;

	@SerializedName("70")
	private JsonMember70 jsonMember70;

	@SerializedName("71")
	private JsonMember71 jsonMember71;

	@SerializedName("72")
	private JsonMember72 jsonMember72;

	@SerializedName("73")
	private JsonMember73 jsonMember73;

	@SerializedName("74")
	private JsonMember74 jsonMember74;

	@SerializedName("75")
	private JsonMember75 jsonMember75;

	@SerializedName("76")
	private JsonMember76 jsonMember76;

	@SerializedName("77")
	private JsonMember77 jsonMember77;

	@SerializedName("78")
	private JsonMember78 jsonMember78;

	@SerializedName("79")
	private JsonMember79 jsonMember79;

	@SerializedName("100")
	private JsonMember100 jsonMember100;

	@SerializedName("101")
	private JsonMember101 jsonMember101;

	@SerializedName("102")
	private JsonMember102 jsonMember102;

	@SerializedName("103")
	private JsonMember103 jsonMember103;

	@SerializedName("80")
	private JsonMember80 jsonMember80;

	@SerializedName("81")
	private JsonMember81 jsonMember81;

	@SerializedName("82")
	private JsonMember82 jsonMember82;

	@SerializedName("83")
	private JsonMember83 jsonMember83;

	@SerializedName("84")
	private JsonMember84 jsonMember84;

	@SerializedName("85")
	private JsonMember85 jsonMember85;

	@SerializedName("86")
	private JsonMember86 jsonMember86;

	@SerializedName("87")
	private JsonMember87 jsonMember87;

	public JsonMember88 getJsonMember88(){
		return jsonMember88;
	}

	public JsonMember89 getJsonMember89(){
		return jsonMember89;
	}

	public JsonMember90 getJsonMember90(){
		return jsonMember90;
	}

	public JsonMember91 getJsonMember91(){
		return jsonMember91;
	}

	public JsonMember92 getJsonMember92(){
		return jsonMember92;
	}

	public JsonMember93 getJsonMember93(){
		return jsonMember93;
	}

	public JsonMember94 getJsonMember94(){
		return jsonMember94;
	}

	public JsonMember95 getJsonMember95(){
		return jsonMember95;
	}

	public JsonMember96 getJsonMember96(){
		return jsonMember96;
	}

	public JsonMember97 getJsonMember97(){
		return jsonMember97;
	}

	public JsonMember10 getJsonMember10(){
		return jsonMember10;
	}

	public JsonMember98 getJsonMember98(){
		return jsonMember98;
	}

	public JsonMember11 getJsonMember11(){
		return jsonMember11;
	}

	public JsonMember99 getJsonMember99(){
		return jsonMember99;
	}

	public JsonMember12 getJsonMember12(){
		return jsonMember12;
	}

	public JsonMember13 getJsonMember13(){
		return jsonMember13;
	}

	public JsonMember14 getJsonMember14(){
		return jsonMember14;
	}

	public JsonMember15 getJsonMember15(){
		return jsonMember15;
	}

	public JsonMember16 getJsonMember16(){
		return jsonMember16;
	}

	public JsonMember17 getJsonMember17(){
		return jsonMember17;
	}

	public JsonMember18 getJsonMember18(){
		return jsonMember18;
	}

	public JsonMember19 getJsonMember19(){
		return jsonMember19;
	}

	public JsonMember0 getJsonMember0(){
		return jsonMember0;
	}

	public JsonMember1 getJsonMember1(){
		return jsonMember1;
	}

	public JsonMember2 getJsonMember2(){
		return jsonMember2;
	}

	public JsonMember3 getJsonMember3(){
		return jsonMember3;
	}

	public JsonMember4 getJsonMember4(){
		return jsonMember4;
	}

	public JsonMember5 getJsonMember5(){
		return jsonMember5;
	}

	public JsonMember6 getJsonMember6(){
		return jsonMember6;
	}

	public JsonMember7 getJsonMember7(){
		return jsonMember7;
	}

	public JsonMember8 getJsonMember8(){
		return jsonMember8;
	}

	public JsonMember9 getJsonMember9(){
		return jsonMember9;
	}

	public JsonMember20 getJsonMember20(){
		return jsonMember20;
	}

	public JsonMember21 getJsonMember21(){
		return jsonMember21;
	}

	public JsonMember22 getJsonMember22(){
		return jsonMember22;
	}

	public JsonMember23 getJsonMember23(){
		return jsonMember23;
	}

	public JsonMember24 getJsonMember24(){
		return jsonMember24;
	}

	public JsonMember25 getJsonMember25(){
		return jsonMember25;
	}

	public JsonMember26 getJsonMember26(){
		return jsonMember26;
	}

	public JsonMember27 getJsonMember27(){
		return jsonMember27;
	}

	public JsonMember28 getJsonMember28(){
		return jsonMember28;
	}

	public JsonMember29 getJsonMember29(){
		return jsonMember29;
	}

	public JsonMember30 getJsonMember30(){
		return jsonMember30;
	}

	public JsonMember31 getJsonMember31(){
		return jsonMember31;
	}

	public JsonMember32 getJsonMember32(){
		return jsonMember32;
	}

	public JsonMember33 getJsonMember33(){
		return jsonMember33;
	}

	public JsonMember34 getJsonMember34(){
		return jsonMember34;
	}

	public JsonMember35 getJsonMember35(){
		return jsonMember35;
	}

	public JsonMember36 getJsonMember36(){
		return jsonMember36;
	}

	public JsonMember37 getJsonMember37(){
		return jsonMember37;
	}

	public JsonMember38 getJsonMember38(){
		return jsonMember38;
	}

	public JsonMember39 getJsonMember39(){
		return jsonMember39;
	}

	public List<CurrencyExchangeItem> getCurrencyExchange(){
		return currencyExchange;
	}

	public JsonMember40 getJsonMember40(){
		return jsonMember40;
	}

	public JsonMember41 getJsonMember41(){
		return jsonMember41;
	}

	public JsonMember42 getJsonMember42(){
		return jsonMember42;
	}

	public JsonMember43 getJsonMember43(){
		return jsonMember43;
	}

	public JsonMember44 getJsonMember44(){
		return jsonMember44;
	}

	public JsonMember45 getJsonMember45(){
		return jsonMember45;
	}

	public JsonMember46 getJsonMember46(){
		return jsonMember46;
	}

	public JsonMember47 getJsonMember47(){
		return jsonMember47;
	}

	public JsonMember48 getJsonMember48(){
		return jsonMember48;
	}

	public JsonMember49 getJsonMember49(){
		return jsonMember49;
	}

	public JsonMember50 getJsonMember50(){
		return jsonMember50;
	}

	public JsonMember51 getJsonMember51(){
		return jsonMember51;
	}

	public JsonMember52 getJsonMember52(){
		return jsonMember52;
	}

	public JsonMember53 getJsonMember53(){
		return jsonMember53;
	}

	public JsonMember54 getJsonMember54(){
		return jsonMember54;
	}

	public JsonMember55 getJsonMember55(){
		return jsonMember55;
	}

	public JsonMember56 getJsonMember56(){
		return jsonMember56;
	}

	public JsonMember57 getJsonMember57(){
		return jsonMember57;
	}

	public JsonMember58 getJsonMember58(){
		return jsonMember58;
	}

	public JsonMember59 getJsonMember59(){
		return jsonMember59;
	}

	public JsonMember60 getJsonMember60(){
		return jsonMember60;
	}

	public JsonMember61 getJsonMember61(){
		return jsonMember61;
	}

	public JsonMember62 getJsonMember62(){
		return jsonMember62;
	}

	public JsonMember63 getJsonMember63(){
		return jsonMember63;
	}

	public JsonMember64 getJsonMember64(){
		return jsonMember64;
	}

	public JsonMember65 getJsonMember65(){
		return jsonMember65;
	}

	public JsonMember66 getJsonMember66(){
		return jsonMember66;
	}

	public JsonMember67 getJsonMember67(){
		return jsonMember67;
	}

	public JsonMember68 getJsonMember68(){
		return jsonMember68;
	}

	public JsonMember69 getJsonMember69(){
		return jsonMember69;
	}

	public JsonMember70 getJsonMember70(){
		return jsonMember70;
	}

	public JsonMember71 getJsonMember71(){
		return jsonMember71;
	}

	public JsonMember72 getJsonMember72(){
		return jsonMember72;
	}

	public JsonMember73 getJsonMember73(){
		return jsonMember73;
	}

	public JsonMember74 getJsonMember74(){
		return jsonMember74;
	}

	public JsonMember75 getJsonMember75(){
		return jsonMember75;
	}

	public JsonMember76 getJsonMember76(){
		return jsonMember76;
	}

	public JsonMember77 getJsonMember77(){
		return jsonMember77;
	}

	public JsonMember78 getJsonMember78(){
		return jsonMember78;
	}

	public JsonMember79 getJsonMember79(){
		return jsonMember79;
	}

	public JsonMember100 getJsonMember100(){
		return jsonMember100;
	}

	public JsonMember101 getJsonMember101(){
		return jsonMember101;
	}

	public JsonMember102 getJsonMember102(){
		return jsonMember102;
	}

	public JsonMember103 getJsonMember103(){
		return jsonMember103;
	}

	public JsonMember80 getJsonMember80(){
		return jsonMember80;
	}

	public JsonMember81 getJsonMember81(){
		return jsonMember81;
	}

	public JsonMember82 getJsonMember82(){
		return jsonMember82;
	}

	public JsonMember83 getJsonMember83(){
		return jsonMember83;
	}

	public JsonMember84 getJsonMember84(){
		return jsonMember84;
	}

	public JsonMember85 getJsonMember85(){
		return jsonMember85;
	}

	public JsonMember86 getJsonMember86(){
		return jsonMember86;
	}

	public JsonMember87 getJsonMember87(){
		return jsonMember87;
	}
}