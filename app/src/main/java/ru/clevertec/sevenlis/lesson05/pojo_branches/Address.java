package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Address{

	@SerializedName("country")
	private String country;

	@SerializedName("streetName")
	private String streetName;

	@SerializedName("townName")
	private String townName;

	@SerializedName("countrySubDivision")
	private String countrySubDivision;

	@SerializedName("buildingNumber")
	private String buildingNumber;

	@SerializedName("description")
	private String description;

	@SerializedName("postCode")
	private String postCode;

	@SerializedName("GeoLocation")
	private GeoLocation geoLocation;

	@SerializedName("department")
	private String department;

	@SerializedName("addressLine")
	private String addressLine;

	public String getCountry(){
		return country;
	}

	public String getStreetName(){
		return streetName;
	}

	public String getTownName(){
		return townName;
	}

	public String getCountrySubDivision(){
		return countrySubDivision;
	}

	public String getBuildingNumber(){
		return buildingNumber;
	}

	public String getDescription(){
		return description;
	}

	public String getPostCode(){
		return postCode;
	}

	public GeoLocation getGeoLocation(){
		return geoLocation;
	}

	public String getDepartment(){
		return department;
	}

	public String getAddressLine(){
		return addressLine;
	}

	public String getAddressName() {
		return getTownName() + ", " + getStreetName() + ", " + getBuildingNumber();
	}
}