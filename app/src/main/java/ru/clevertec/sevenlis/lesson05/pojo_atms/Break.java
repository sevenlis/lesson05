
package ru.clevertec.sevenlis.lesson05.pojo_atms;


import com.google.gson.annotations.SerializedName;

public class Break {

    @SerializedName("breakFromTime")
    public String breakFromTime;
    @SerializedName("breakToTime")
    public String breakToTime;

    public String getBreakFromTime() {
        return breakFromTime;
    }

    public String getBreakToTime() {
        return breakToTime;
    }
}
