package ru.clevertec.sevenlis.lesson05;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public final class RootItemObserver<T> implements Observer<T>, Disposable {

    final Observer<? super RootItem> downstream;

    private Disposable upstream;

    public RootItemObserver(Observer<? super RootItem> downstream) {
        this.downstream = downstream;
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        if (upstream != null) {
            d.dispose();
        } else {
            upstream = d;
            downstream.onSubscribe(this);
        }
    }

    @Override
    public void onNext(@NonNull T t) {
        downstream.onNext((RootItem) t);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        downstream.onError(e);
    }

    @Override
    public void onComplete() {
        downstream.onComplete();
    }

    @Override
    public void dispose() {
        upstream.dispose();
    }

    @Override
    public boolean isDisposed() {
        return upstream.isDisposed();
    }
}
