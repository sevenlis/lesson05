package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class BranchItem{

	@SerializedName("branchId")
	private String branchId;

	@SerializedName("Services")
	private Services services;

	@SerializedName("wifi")
	private int wifi;

	@SerializedName("equeue")
	private int equeue;

	@SerializedName("Address")
	private Address address;

	@SerializedName("name")
	private String name;

	@SerializedName("Information")
	private Information information;

	@SerializedName("Accessibilities")
	private Accessibilities accessibilities;

	public String getBranchId(){
		return branchId;
	}

	public Services getServices(){
		return services;
	}

	public int getWifi(){
		return wifi;
	}

	public int getEqueue(){
		return equeue;
	}

	public Address getAddress(){
		return address;
	}

	public String getName(){
		return name;
	}

	public Information getInformation(){
		return information;
	}

	public Accessibilities getAccessibilities(){
		return accessibilities;
	}
}