package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Accessibility{

	@SerializedName("description")
	private String description;

	@SerializedName("type")
	private String type;

	public String getDescription(){
		return description;
	}

	public String getType(){
		return type;
	}
}