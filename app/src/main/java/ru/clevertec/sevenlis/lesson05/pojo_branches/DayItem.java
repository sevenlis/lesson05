package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class DayItem{

	@SerializedName("closingTime")
	private String closingTime;

	@SerializedName("dayCode")
	private int dayCode;

	@SerializedName("openingTime")
	private String openingTime;

	@SerializedName("Break")
	private Break _break;

	public String getClosingTime(){
		return closingTime;
	}

	public int getDayCode(){
		return dayCode;
	}

	public String getOpeningTime(){
		return openingTime;
	}

	public Break getBreak(){
		return _break;
	}
}