package ru.clevertec.sevenlis.lesson05;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        showMapFragment();
    }

    private void showMapFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .disallowAddToBackStack()
                .replace(R.id.fragment_container_view, MapFragment.class, null, MapFragment.class.getSimpleName())
                .commit();
    }
}