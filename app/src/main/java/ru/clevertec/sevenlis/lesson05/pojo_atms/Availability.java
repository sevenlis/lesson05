
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

public class Availability {

    @SerializedName("access24Hours")
    public Boolean access24Hours;
    @SerializedName("isRestricted")
    public Boolean isRestricted;
    @SerializedName("sameAsOrganization")
    public Boolean sameAsOrganization;
    @SerializedName("StandardAvailability")
    public StandardAvailability standardAvailability;

    public Boolean getAccess24Hours() {
        return access24Hours;
    }

    public Boolean getRestricted() {
        return isRestricted;
    }

    public Boolean getSameAsOrganization() {
        return sameAsOrganization;
    }

    public StandardAvailability getStandardAvailability() {
        return standardAvailability;
    }
}
