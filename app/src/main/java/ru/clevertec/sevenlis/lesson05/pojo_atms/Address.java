
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("streetName")
    public String streetName;
    @SerializedName("buildingNumber")
    public String buildingNumber;
    @SerializedName("townName")
    public String townName;
    @SerializedName("countrySubDivision")
    public String countrySubDivision;
    @SerializedName("country")
    public String country;
    @SerializedName("addressLine")
    public String addressLine;
    @SerializedName("description")
    public String description;
    @SerializedName("Geolocation")
    public Geolocation geolocation;

    public String getStreetName() {
        return streetName;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public String getTownName() {
        return townName;
    }

    public String getCountrySubDivision() {
        return countrySubDivision;
    }

    public String getCountry() {
        return country;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public String getDescription() {
        return description;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public String getAddressName() {
        return getTownName() + ", " + getStreetName() + ", " + getBuildingNumber();
    }
}
