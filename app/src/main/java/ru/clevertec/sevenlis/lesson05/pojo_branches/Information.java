package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Information{

	@SerializedName("Availability")
	private Availability availability;

	@SerializedName("ContactDetails")
	private ContactDetails contactDetails;

	@SerializedName("segment")
	private String segment;

	public Availability getAvailability(){
		return availability;
	}

	public ContactDetails getContactDetails(){
		return contactDetails;
	}

	public String getSegment(){
		return segment;
	}
}