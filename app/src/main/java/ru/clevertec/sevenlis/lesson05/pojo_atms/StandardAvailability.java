
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StandardAvailability {

    @SerializedName("Day")
    public List<Day> days = null;

    public List<Day> getDays() {
        return days;
    }
}
