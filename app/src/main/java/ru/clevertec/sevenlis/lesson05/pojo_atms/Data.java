
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("ATM")
    public List<Atm> atms = new ArrayList<>();

    public List<Atm> getAtms() {
        return atms;
    }
}
