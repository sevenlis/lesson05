package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Links{

	@SerializedName("Self")
	private String self;

	public String getSelf(){
		return self;
	}
}