package ru.clevertec.sevenlis.lesson05;

import java.util.List;

public interface RootItem {
    List<MarkerItem> getMarkerItems();
}
