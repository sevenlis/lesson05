package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class GeoLocation{

	@SerializedName("GeographicCoordinates")
	private GeographicCoordinates geographicCoordinates;

	public GeographicCoordinates getGeographicCoordinates(){
		return geographicCoordinates;
	}
}