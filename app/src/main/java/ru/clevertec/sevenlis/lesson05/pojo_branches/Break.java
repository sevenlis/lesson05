package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

public class Break{

	@SerializedName("breakFromTime")
	private String breakFromTime;

	@SerializedName("breakToTime")
	private String breakToTime;

	public String getBreakFromTime(){
		return breakFromTime;
	}

	public String getBreakToTime(){
		return breakToTime;
	}
}