package ru.clevertec.sevenlis.lesson05;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private static final int MARKER_COUNT = 10;
    private final List<MarkerItem> markerItems = new ArrayList<>();
    private final LatLng START_POINT = new LatLng(52.425163D, 31.015039D);
    private GoogleMap mMap;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        UiSettings settings = googleMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setMyLocationButtonEnabled(true);

        mMap.addMarker(new MarkerOptions()
                .title("Start position")
                .snippet(START_POINT.latitude + " " + START_POINT.longitude)
                .position(START_POINT)
        );
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(START_POINT,15.0f),1000,null);

        Observer<RootItem> rootsObserver = new Observer<RootItem>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                markerItems.clear();
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull RootItem itemRoot) {
                for (MarkerItem markerItem : itemRoot.getMarkerItems()) {
                    markerItem.setDistance(START_POINT);
                    markerItems.add(markerItem);
                }
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {}

            @Override
            public void onComplete() {
                Comparator<MarkerItem> distanceComparator = new Comparator<MarkerItem>() {
                    @Override
                    public int compare(MarkerItem markerItem1, MarkerItem markerItem2) {
                        return Double.compare(markerItem1.getDistance(), markerItem2.getDistance());
                    }
                };
                Collections.sort(markerItems, distanceComparator);
                addMarkers(markerItems);
            }
        };

        ApiService apiService = ApiServiceHelper.getInstance().getApiService();

        //объединяем только 2 потока, поскольку у api v1.0 нет методов отдельно получить банкоматы и отдельно инфокиоски
        Observable.concat(apiService.loadAtms(), apiService.loadBranches())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rootsObserver);

        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(@NonNull Marker marker) {
                Toast.makeText(getActivity(), marker.getTitle() + "\n" + marker.getSnippet(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addMarkers(List<MarkerItem> markerItems) {
        for (MarkerItem markerItem : markerItems.subList(0, MARKER_COUNT)) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(markerItem.getIconDescriptor())
                    .position(markerItem.getLatLng())
                    .anchor(0.5f, 1.0f)
                    .title(markerItem.getAddress())
                    .snippet(markerItem.getDescription() + " " + markerItem.getType());

            mMap.addMarker(markerOptions);
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 20.0f),1000,null);
        marker.showInfoWindow();
        return true;
    }


}
