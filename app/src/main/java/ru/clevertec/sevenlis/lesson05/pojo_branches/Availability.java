package ru.clevertec.sevenlis.lesson05.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Availability{

	@SerializedName("StandardAvailability")
	private StandardAvailability standardAvailability;

	@SerializedName("sameAsOrganization")
	private int sameAsOrganization;

	@SerializedName("description")
	private String description;

	@SerializedName("NonStandardAvailability")
	private List<Object> nonStandardAvailability;

	@SerializedName("isRestricted")
	private int isRestricted;

	@SerializedName("access24Hours")
	private int access24Hours;

	public StandardAvailability getStandardAvailability(){
		return standardAvailability;
	}

	public int getSameAsOrganization(){
		return sameAsOrganization;
	}

	public String getDescription(){
		return description;
	}

	public List<Object> getNonStandardAvailability(){
		return nonStandardAvailability;
	}

	public int getIsRestricted(){
		return isRestricted;
	}

	public int getAccess24Hours(){
		return access24Hours;
	}
}