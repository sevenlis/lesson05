package ru.clevertec.sevenlis.lesson05.pojo_branches;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ContactDetails{

	@SerializedName("emailAddress")
	private String emailAddress;

	@SerializedName("other")
	private String other;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("mobileNumber")
	private String mobileNumber;

	@SerializedName("name")
	private String name;

	@SerializedName("faxNumber")
	private String faxNumber;

	@SerializedName("SocialNetworks")
	private List<SocialNetworksItem> socialNetworks;

	public String getEmailAddress(){
		return emailAddress;
	}

	public String getOther(){
		return other;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public String getName(){
		return name;
	}

	public String getFaxNumber(){
		return faxNumber;
	}

	public List<SocialNetworksItem> getSocialNetworks(){
		return socialNetworks;
	}
}