package ru.clevertec.sevenlis.lesson05.pojo_branches;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.clevertec.sevenlis.lesson05.RootItem;
import ru.clevertec.sevenlis.lesson05.MarkerItem;

public class BranchesRoot implements RootItem {

	@SerializedName("Meta")
	private Meta meta;

	@SerializedName("Links")
	private Links links;

	@SerializedName("Data")
	private Data data;

	public Meta getMeta(){
		return meta;
	}

	public Links getLinks(){
		return links;
	}

	public Data getData(){
		return data;
	}

	@Override
	public List<MarkerItem> getMarkerItems() {
		List<MarkerItem> markerItems = new ArrayList<>();
		for (BranchItem branchItem : getData().getBranches()) {
			markerItems.add(new MarkerItem(branchItem));
		}
		return markerItems;
	}
}