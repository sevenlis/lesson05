
package ru.clevertec.sevenlis.lesson05.pojo_atms;

import com.google.gson.annotations.SerializedName;

public class Service {

    @SerializedName("serviceType")
    public String serviceType;
    @SerializedName("description")
    public String description;

    public String getServiceType() {
        return serviceType;
    }

    public String getDescription() {
        return description;
    }
}
